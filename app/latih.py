from app import app
from flask import Flask
import cv2, os, numpy as np
from PIL import Image

wajahDir = './images'
latihDir = './latihwajah'

faceDetector = cv2.CascadeClassifier('./includes/haarcascade_frontalface_default.xml')
eyeDetector  = cv2.CascadeClassifier('./includes/haarcascade_eye.xml')

def getImageLabel(path):
    imagePaths = [os.path.join(path,f) for f in os.listdir(path)]
    faceSamples = []
    faceIDs = []
    for imagePath in imagePaths:
        PILImg = Image.open(imagePath).convert('L') #convert kedalam gray
        imgNum = np.array(PILImg, 'uint8')
        faceID = int(os.path.split(imagePath)[-1].split(".")[1])
        faces  = faceDetector.detectMultiScale(imgNum)
        for (x, y, w, h) in faces:
            faceSamples.append(imgNum[y:y+h, x:x+w])
            faceIDs.append(faceID)
        return faceSamples, faceIDs

faceRecognizer = cv2.face.LBPHFaceRecognizer_create()

print("Mesin sedang melakukan training data wajah. Tunggu...")
faces, IDs = getImageLabel(wajahDir)
faceRecognizer.train(faces, np.array(IDs))

# save
faceRecognizer.write(latihDir+'/training.xml')
print("Sebanyak {0} data wajah telah dilatih ke mesin", format(len(np.unique(IDs))))
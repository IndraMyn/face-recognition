from app import app
from flask import Flask
import cv2

cam = cv2.VideoCapture(0) #urutan webcam
while True:

    retV, frame = cam.read()
    # Mengubah jadi abu abu
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Menampilkan Frame
    cv2.imshow('webcamku', frame)
    # cv2.imshow('webcamku', gray)

    # Mematikan looping dengan menekan keyboard (27 = esc)
    k = cv2.waitKey(1) & 0xFF
    if k == 27 or k == ord('q'):
        break

# Menghapus memori cam
cam.release()
cv2.destroyAllWindows()
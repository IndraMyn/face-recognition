from app import app
from flask import Flask
import cv2, os, numpy as np

wajahDir = './images'
latihDir = './latihwajah'

cam = cv2.VideoCapture(0) #urutan webcam

# Mengatur besar cam
cam.set(3, 640) #Mengatur lebar (3)
cam.set(4, 489) #Mengatur tinggi (4)

faceDetector = cv2.CascadeClassifier('./includes/haarcascade_frontalface_default.xml')
faceRecognizer = cv2.face.LBPHFaceRecognizer_create()

faceRecognizer.read(latihDir+'/training.xml')
font = cv2.FONT_HERSHEY_SIMPLEX

id = 0
names = ['Tidak Diketahui', 'Indra Mulyawan', 'Nama Lain']

minWidth = 0.1*cam.get(3)
minHeight = 0.1*cam.get(4)

while True:

    retV, frame = cam.read()
    frame = cv2.flip(frame, 1) #vertical flip
    # Mengubah jadi abu abu
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    faces = faceDetector.detectMultiScale(gray, 1.2, 5, minSize=(round(minWidth), round(minHeight)))

    # deteksi wajah
    for (x, y, w, h) in faces:
        frame = cv2.rectangle(frame, (x,y), (x+w, y+h), (0,0,255), 2)
        
        id, confidence = faceRecognizer.predict(gray[y:y+h, x:x+w])

        if confidence <= 50:
            nameId = names[id]
            confidenceTxt = "{0}%".format(round(100-confidence))
        else:
            nameId = names[0]
            confidenceTxt = "{0}%".format(round(100-confidence))

        cv2.putText(frame,str(nameId),(x+5, y-5), font, 1, (255,255,255), 2)
        cv2.putText(frame,str(confidenceTxt),(x+5, y+h-5), font, 1, (255,255,255), 1)

        
    # Menampilkan Frame
    cv2.imshow('Recoginition wajah', frame)
    # cv2.imshow('webcamku', gray)

    # Mematikan looping dengan menekan keyboard (27 = esc)
    k = cv2.waitKey(1) & 0xFF
    if k == 27 or k == ord('q'):
        break

print('Exit')
# Menghapus memori cam
cam.release()
cv2.destroyAllWindows()
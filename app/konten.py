from flask import Flask
from app import app
import cv2, os, numpy as np
from PIL import Image

cam = cv2.VideoCapture(0) #urutan webcam

# Mengatur besar cam
cam.set(3, 1920) #Mengatur lebar (3)
cam.set(4, 1080) #Mengatur tinggi (4)

wajahDir = './images'
latihDir = './latihwajah'

faceDetector = cv2.CascadeClassifier('./includes/haarcascade_frontalface_default.xml')
eyeDetector  = cv2.CascadeClassifier('./includes/haarcascade_eye.xml')

def webcam():
    while True:

        retV, frame = cam.read()
        # Mengubah jadi abu abu
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Menampilkan Frame
        cv2.imshow('webcamku', frame)
        # cv2.imshow('webcamku', gray)

        # Mematikan looping dengan menekan keyboard (27 = esc)
        k = cv2.waitKey(1) & 0xFF
        if k == 27 or k == ord('q'):
            break

    # Menghapus memori cam
    cam.release()
    cv2.destroyAllWindows()

def deteksi():

    while True:

        retV, frame = cam.read()
        # Mengubah jadi abu abu
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
        faces = faceDetector.detectMultiScale(gray, 1.3, 5)

        # deteksi wajah
        for (x, y, w, h) in faces:
            frame = cv2.rectangle(frame, (x,y), (x+w, y+h), (0,0,255), 2)

            # Deteksi Mata
            roiGray     = gray[y:y+h, x:x+w]
            roiColorful = frame[y:y+h, x:x+w]
            eyes = eyeDetector.detectMultiScale(roiGray)
            for (xe, ye, we, he) in eyes:
                cv2.rectangle(roiColorful, (xe,ye), (xe+we, ye+he), (255,255,255),1)


        # Menampilkan Frame
        cv2.imshow('webcamku', frame)
        # cv2.imshow('webcamku', gray)

        # Mematikan looping dengan menekan keyboard (27 = esc)
        k = cv2.waitKey(1) & 0xFF
        if k == 27 or k == ord('q'):
            break

    # Menghapus memori cam
    cam.release()
    cv2.destroyAllWindows()

def latih():

    def getImageLabel(path):
        imagePaths = [os.path.join(path,f) for f in os.listdir(path)]
        # print(imagePaths)
        faceSamples = []
        faceIDs = []
        for imagePath in imagePaths:
            PILImg = Image.open(imagePath).convert('L') #convert kedalam gray
            imgNum = np.array(PILImg, 'uint8')
            faceID = int(os.path.split(imagePath)[-1].split(".")[1])
            print(faceID)
            # print(faceID)
            faces  = faceDetector.detectMultiScale(imgNum)
            for (x, y, w, h) in faces:
                faceSamples.append(imgNum[y:y+h,x:x+w])
                faceIDs.append(faceID)
            return faceSamples, faceIDs

    faceRecognizer = cv2.face.LBPHFaceRecognizer_create()

    print("Mesin sedang melakukan training data wajah. Tunggu...")
    faces, IDs = getImageLabel(wajahDir)
    faceRecognizer.train(faces, np.array(IDs))

    # save
    faceRecognizer.write(latihDir+'/training.xml')
    print("Sebanyak {0} data wajah telah dilatih ke mesin", format(len(np.unique(IDs))))

def daftar():

    namaUser     = input("Masukkan nama anda : ")
    faceId       = input("Masukkan Face ID yang akan direkam : ")
    print("Harap liat ke webcam. Tunggu proses selesai...")
    ambilData    = 1

    while True:

        retV, frame = cam.read()
        # Mengubah jadi abu abu
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
        faces = faceDetector.detectMultiScale(gray, 1.3, 5)

        # deteksi wajah
        for (x, y, w, h) in faces:
            frame = cv2.rectangle(frame, (x,y), (x+w, y+h), (0,0,255), 2)
            namaFile = 'wajah.'+str(faceId)+'.'+str(namaUser)+'.'+str(ambilData)+'.jpg'
            cv2.imwrite(wajahDir+'/'+namaFile, frame)
            ambilData += 1

            # # Deteksi Mata
            # roiGray     = gray[y:y+h, x:x+w]
            # roiColorful = frame[y:y+h, x:x+w]
            # eyes = eyeDetector.detectMultiScale(roiGray)

            # for (xe, ye, we, he) in eyes:
            #     frame = cv2.rectangle(roiColorful, (xe,ye), (xe+we, ye+he), (255,255,255),1)
            #     namaFile = 'mata.'+str(faceId)+'.'+str(ambilData)+'.jpg'
            #     cv2.imwrite(wajahDir+'/'+namaFile, frame)
            #     ambilData += 1


        # Menampilkan Frame
        cv2.imshow('webcamku', frame)
        # cv2.imshow('webcamku', gray)

        # Mematikan looping dengan menekan keyboard (27 = esc)
        k = cv2.waitKey(1) & 0xFF
        if k == 27 or k == ord('q'):
            break
        elif ambilData >= 30:
            break

    print('Pengambil data selasai')
    # Menghapus memori cam
    cam.release()
    cv2.destroyAllWindows()

    # Melatih hasil rekaman
    latih()

def recognition():

    faceRecognizer = cv2.face.LBPHFaceRecognizer_create()
    faceRecognizer.read(latihDir+'/training.xml')
    font = cv2.FONT_HERSHEY_SIMPLEX

    id = ''
    # names = ['Tidak Diketahui', 'Dikenali']

    # minWidth = 0.1*cam.get(3)
    # minHeight = 0.1*cam.get(4)

    while True:

        retV, frame = cam.read()
        frame = cv2.flip(frame, 1) #vertical flip
        # Mengubah jadi abu abu
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
        faces = faceDetector.detectMultiScale(gray, 1.2, 5 )#, minSize=(round(minWidth), round(minHeight)))

        # deteksi wajah
        for (x, y, w, h) in faces:
            frame = cv2.rectangle(frame, (x,y), (x+w, y+h), (0,0,255), 2)
            
            id, confidence = faceRecognizer.predict(gray[y:y+h,x:x+w])

            # Deteksi Mata
            roiGray     = gray[y:y+h, x:x+w]
            roiColorful = frame[y:y+h, x:x+w]
            eyes = eyeDetector.detectMultiScale(roiGray)
            for (xe, ye, we, he) in eyes:
                cv2.rectangle(roiColorful, (xe,ye), (xe+we, ye+he), (255,255,255),1)

                if confidence <= 60:
                    nameId =  id #names[id]
                    confidenceTxt = "{0}%".format(round(100-confidence))
                else:
                    nameId = "Tidak diketahui" #names[0]
                    confidenceTxt = "{0}%".format(round(100-confidence))

                cv2.putText(frame,str(nameId),(x+5, y-5), font, 1, (255,255,255), 2)
                cv2.putText(frame,str(confidenceTxt),(x+5, y+h-5), font, 1, (255,255,255), 1)

            
        # Menampilkan Frame
        cv2.imshow('Recoginition wajah', frame)
        # cv2.imshow('webcamku', gray)

        # Mematikan looping dengan menekan keyboard (27 = esc)
        k = cv2.waitKey(1) & 0xFF
        if k == 27 or k == ord('q'):
            break

    print('Exit')
    # Menghapus memori cam
    cam.release()
    cv2.destroyAllWindows()

# Perintah
command = input("list command: \n webcam \n deteksi \n rekam \n recognition \n Apa yang anda ingin lakukan ?")

if command == "webcam":
    webcam()
elif command == "deteksi":
    deteksi()
elif command == "daftar":
    daftar()
elif command == "recognition":
    recognition()
elif command == "latih":
    latih()
else:
    print("Command undefined \n list command: \n webcam \n deteksi \n rekam \n recognition")
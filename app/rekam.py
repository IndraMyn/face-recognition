from app import app
from flask import Flask
import cv2, os

cam = cv2.VideoCapture(0) #urutan webcam

# Mengatur besar cam
cam.set(3, 640) #Mengatur lebar (3)
cam.set(4, 489) #Mengatur tinggi (4)

wajahDir = './images'

faceDetector = cv2.CascadeClassifier('./includes/haarcascade_frontalface_default.xml')
eyeDetector  = cv2.CascadeClassifier('./includes/haarcascade_eye.xml')
faceId       = input("Masukkan Face ID yang akan direkam : ")
print("Harap liat ke webcam. Tunggu proses selesai...")
ambilData    = 1

while True:

    retV, frame = cam.read()
    # Mengubah jadi abu abu
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    faces = faceDetector.detectMultiScale(gray, 1.3, 5)

    # deteksi wajah
    for (x, y, w, h) in faces:
        frame = cv2.rectangle(frame, (x,y), (x+w, y+h), (0,0,255), 2)

        namaFile = 'wajah.'+str(faceId)+'.'+str(ambilData)+'.jpg'
        cv2.imwrite(wajahDir+'/'+namaFile, frame)
        ambilData += 1

        # # Deteksi Mata
        # roiGray     = gray[y:y+h, x:x+w]
        # roiColorful = frame[y:y+h, x:x+w]
        # eyes = eyeDetector.detectMultiScale()
        # for (xe, ye, we, he) in eyes:
        #     cv2.rectangle(roiColorful, (xe,ye), (xe+we, ye+he), (0,0,255),1)


    # Menampilkan Frame
    cv2.imshow('webcamku', frame)
    # cv2.imshow('webcamku', gray)

    # Mematikan looping dengan menekan keyboard (27 = esc)
    k = cv2.waitKey(1) & 0xFF
    if k == 27 or k == ord('q'):
        break
    elif ambilData >= 30:
        break

print('Pengambil data selasai')
# Menghapus memori cam
cam.release()
cv2.destroyAllWindows()